package com.jgibson.ontimeair.models
{
	public class LoadStatus
	{
		public static const GHOST:LoadStatus = new LoadStatus("GHOST");
		public static const LOADING:LoadStatus = new LoadStatus("LOADING");
		public static const LOADED:LoadStatus = new LoadStatus("LOADED");
		public static const ERROR:LoadStatus = new LoadStatus("ERROR");
		private var _type:String;
		
		public function LoadStatus(type:String)
		{
			_type = type;
		}
	}
}