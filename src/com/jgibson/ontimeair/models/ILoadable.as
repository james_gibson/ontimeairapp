package com.jgibson.ontimeair.models
{
	import org.osflash.signals.ISignal;
	
	public interface ILoadable
	{
		function get loadStatus():LoadStatus;
		function get loading():ISignal;
		function get loaded():ISignal;
	}
}