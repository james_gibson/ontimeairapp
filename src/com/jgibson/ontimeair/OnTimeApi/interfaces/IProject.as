package com.jgibson.ontimeair.OnTimeApi.interfaces
{
	public interface IProject
	{
	/*	_id = attributes["id"];	
		_name = attributes["name"];
		_canModify = attributes["can_modify"];
		_description = attributes["description"];
		_startDate = attributes["start_date"];
		_dueDate = attributes["due_date"];
		_isActive = attributes["is_active"];
		_parent = attributes["parent"];
		_releases = attributes["releases"];
		_children = attributes["children"];*/
		
		function get id():String;
		function set id(value:String):void;
		function get name():String;
		function get canModify():Boolean;
		function get description():String;
		function get startDate():Date;
		function get dueDate():Date;
		function get isActive():Boolean;
		function get parent():IProject;
		function get releases():Object;
		function get children():Object;
	}
}