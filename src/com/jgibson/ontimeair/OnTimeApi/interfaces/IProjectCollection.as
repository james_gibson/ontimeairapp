package com.jgibson.ontimeair.OnTimeApi.interfaces
{
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;
	
	import mx.collections.ArrayCollection;

	public interface IProjectCollection
	{
		function load(api:IOnTime):void;
		function externalLoad(collection:ArrayCollection):void;
	}
}