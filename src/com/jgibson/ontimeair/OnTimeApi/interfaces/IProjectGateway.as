package com.jgibson.ontimeair.OnTimeApi.interfaces
{
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;

	public interface IProjectGateway
	{
		function load(projectId:String, api:IOnTime):void;
	}
}