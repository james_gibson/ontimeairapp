package com.jgibson.ontimeair.OnTimeApi
{
	public interface ISettings
	{
		/* Properties */
		function get clientId():String;
		function get clientSecret():String;
		function get onTimeUrl():String;
		function get loginInfo():ILogin;
		function set loginInfo(value:ILogin):void;
		
	}
}