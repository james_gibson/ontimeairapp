package com.jgibson.ontimeair.OnTimeApi.gateways
{
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;
	import com.jgibson.ontimeair.OnTimeApi.interfaces.IProjectGateway;
	import com.jgibson.ontimeair.models.ILoadable;
	import com.jgibson.ontimeair.models.LoadStatus;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ArrayUtil;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;

	public class ProjectGateway implements IProjectGateway, ILoadable
	{
		private var _api:IOnTime;
		private var _projectId:String;
		private var _loaded:ISignal;
		private var _loading:ISignal;
		private var _loadStatus:LoadStatus;
		public function ProjectGateway()
		{
			
			/* This class is focused on version 1 of the api*/
			
			_loading = new Signal();
			_loaded = new Signal(ArrayCollection);
			_loadStatus = LoadStatus.GHOST;
		}
		
		public function load(projectId:String, api:IOnTime):void
		{
			_api = api;
			_projectId = projectId;
			
			executeProjectQuery();
		}
		
		private function executeProjectQuery():void
		{
			trace("Requesting Project(s)");
			_loadStatus = LoadStatus.LOADING;
			loading.dispatch();
			
			var loader:URLLoader = new URLLoader();
			configureListeners(loader);
			var postRequestVariables:URLVariables = new URLVariables();
			postRequestVariables.access_token = _api.sessionData.accessToken;
			
			
			var postRequest:URLRequest = new URLRequest();
			postRequest.url = _api.settings.onTimeUrl +"/v1/projects/";
			trace(postRequest.url);
			postRequest.data = postRequestVariables;
			postRequest.contentType = "application/json";
			postRequest.method = URLRequestMethod.GET;
			try {
				
				loader.load(postRequest);
			} catch (error:Error) {
				trace("Unable to load requested document.");
			}
			
			
		}
		
		private function configureListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			/*dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);*/
			dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			/*dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);*/
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		}
		
		private function completeHandler(event:Event):void {
			var loader:URLLoader = URLLoader(event.target)
			loader.dataFormat = URLLoaderDataFormat.VARIABLES;
			trace("completeHandler: " + loader.data);
			
			if (loader.data != null)
			{
				var attributes:ArrayCollection = new ArrayCollection(ArrayUtil.toArray(JSON.parse(loader.data)));
				if (attributes[0] != null)
				{
					_loadStatus = LoadStatus.LOADED;
					loaded.dispatch(new ArrayCollection(attributes[0].data));
				}
				
			}
			
		}
		
		
		private function openHandler(event:Event):void {
			trace("openHandler: " + event);
		}
		
		private function progressHandler(event:ProgressEvent):void {
			trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("securityErrorHandler: " + event);
		}
		
		private function httpStatusHandler(event:HTTPStatusEvent):void {
			trace("httpStatusHandler: " + event);
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
			trace("ioErrorHandler: " + event);
		}
		
		public function get loadStatus():LoadStatus
		{
			return _loadStatus;
		}
		
		public function get loaded():ISignal
		{
			return _loaded;
		}
		
		public function get loading():ISignal
		{
			return _loading;
		}
		
	}
}