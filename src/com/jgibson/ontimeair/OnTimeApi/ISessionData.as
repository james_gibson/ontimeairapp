package com.jgibson.ontimeair.OnTimeApi
{
	public interface ISessionData
	{
		function get accessToken():String;
		function get firstName():String;
		function get lastName():String;
		function get email():String;
		function get userId():String;
		function get tokenType():String;
	}
}