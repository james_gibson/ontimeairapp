package com.jgibson.ontimeair.OnTimeApi
{
	import com.jgibson.ontimeair.models.ILoadable;
	import com.jgibson.ontimeair.models.LoadStatus;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ArrayUtil;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	public class OnTime implements IOnTime, ILoadable
	{
		private var _settings:ISettings;
		private var _sessionData:ISessionData;
		private var _accessToken:String;
		private var _loadStatus:LoadStatus;
		private var _loaded:ISignal;
		private var _loading:ISignal;
		public function OnTime()
		{
			_loadStatus = LoadStatus.GHOST;
			_loading = new Signal();
			_loaded = new Signal();
		}
		
		public function set settings(settings:ISettings):void
		{
			_settings = settings;
		}
			
		[Bindable]
		public function get settings():ISettings
		{
			return _settings;
		}
		
		/* CRUD operations should likely be moved into a sub class */
		public function Create():void
		{
			// TODO Auto Generated method stub
			
		}
		
		public function Delete(resource:String, id:int):void
		{
			// TODO Auto Generated method stub
			
		}
		
		public function Read():void
		{
			// TODO Auto Generated method stub
			
		}
		
		public function Update():void
		{
			// TODO Auto Generated method stub
			
		}
		
		public function get hasAccessToken():Boolean
		{
			return _accessToken != null;
		}
		
		private function obtainAccessToken():void
		{
			if (settings == null)
			{
				_loadStatus = LoadStatus.ERROR;
				return;
			}
			
			trace("Obtaining Access Token");
			_loadStatus = LoadStatus.LOADING;
			loading.dispatch();
			
			var loader:URLLoader = new URLLoader();
			configureAccessTokenListeners(loader);
			var postRequestVariables:URLVariables = new URLVariables();
			postRequestVariables.grant_type = "password";
			postRequestVariables.client_id = settings.clientId;
			postRequestVariables.client_secret = settings.clientSecret;
			postRequestVariables.username = settings.loginInfo.username;
			postRequestVariables.password = settings.loginInfo.password;
			
			var postRequest:URLRequest = new URLRequest();
			postRequest.url = _settings.onTimeUrl + "/oauth2/token";
			postRequest.data = postRequestVariables;
			postRequest.contentType = "application/json";
			postRequest.method = URLRequestMethod.GET;
			try {
				loader.load(postRequest);
			} catch (error:Error) {
				trace("Unable to load requested document.");
			}
				
				
				
		}
		
		
		public function obtainAccessTokenFromUsernamePassword():void
		{
			obtainAccessToken();			
		}
		
		public function get sessionData():ISessionData
		{
			return _sessionData;
		}
		
		/* HTTP methods */
		
		private function get(url:String, parameters:String):void
		{
			trace("Nothing to see here...We should trash this");			
		}
		
		private function configureAccessTokenListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
			dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
		}
		
		private function completeHandler(event:Event):void {
			var loader:URLLoader = URLLoader(event.target)
				loader.dataFormat = URLLoaderDataFormat.VARIABLES;
			trace("completeHandler: " + loader.data);
			_sessionData = new SessionData(JSON.parse(loader.data));
			
			if (_sessionData.accessToken != null)
			{
				_accessToken = _sessionData.accessToken;
				_loadStatus = LoadStatus.LOADED;
				loaded.dispatch();
			}
			var attributes:ArrayCollection = new ArrayCollection(ArrayUtil.toArray(JSON.parse(loader.data)));
		}
		
		private function openHandler(event:Event):void {
			trace("openHandler: " + event);
		}
		
		private function progressHandler(event:ProgressEvent):void {
			trace("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
		}
		
		private function securityErrorHandler(event:SecurityErrorEvent):void {
			trace("securityErrorHandler: " + event);
		}
		
		private function httpStatusHandler(event:HTTPStatusEvent):void {
			trace("httpStatusHandler: " + event);
		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
			trace("ioErrorHandler: " + event);
		}
		
		public function get loadStatus():LoadStatus
		{
			return _loadStatus;
		}
		
		public function get loaded():ISignal
		{
			return _loaded;
		}
		
		public function get loading():ISignal
		{
			return _loading;
		}
		
		
	}
}