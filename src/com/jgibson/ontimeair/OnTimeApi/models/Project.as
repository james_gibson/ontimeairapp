package com.jgibson.ontimeair.OnTimeApi.models
{
	import com.jgibson.ontimeair.OnTimeApi.interfaces.IProject;
	import com.jgibson.ontimeair.OnTimeApi.interfaces.IProjectCollection;
	import com.jgibson.ontimeair.models.LoadStatus;
	import mx.utils.ArrayUtil;
	import mx.collections.ArrayCollection;

	public class Project implements IProject
	{
		private var _id:String;
		private var _name:String;
		private var _canModify:Object;
		private var _description:String;
		private var _startDate:Date;
		private var _dueDate:Date;
		private var _isActive:Boolean;
		private var _releases:Object;
		private var _parent:IProject;
		private var _children:IProjectCollection;
		
		private var _loadStatus:LoadStatus;
		private var _parentId:String;
		public function Project(projectData:Object)
		{
			_loadStatus = LoadStatus.GHOST;
			
			
			var attributes:Object = projectData;
			
			/* Version one project response data
			{
			"id": 1,
			"name": "New Product",
			"can_modify": true,
			"description": "",
			"start_date": "1899-01-01T05:00:00Z",
			"due_date": "1899-01-01T05:00:00Z",
			"is_active": true,
			"parent": 	{
			"id": 0
			},
			"releases": {
			"inherits": true
			},
			"children": []
			}*/
			
			_id = attributes["id"];	
			_name = attributes["name"];
			_canModify = new Boolean(attributes["can_modify"]);
			_description = attributes["description"];
			_startDate = new Date(attributes["start_date"]);
			_dueDate = new Date(attributes["due_date"]);
			_isActive = new Boolean(attributes["is_active"]);
			_parentId = attributes["parent"].id;
			_releases = attributes["releases"];
			_children = new ProjectCollection();
			_children.externalLoad(new ArrayCollection(ArrayUtil.toArray(attributes["children"])));
		}
		
		public function get canModify():Boolean
		{
			return _canModify;
		}
		
		public function get children():Object
		{
			return null;
		}
		
		public function get description():String
		{
			return _description;
		}
		
		public function get dueDate():Date
		{
			return _dueDate;
		}
		
		public function get id():String
		{
			return _id;
		}
		
		public function set id(value:String):void
		{
			_id = value;
		}
		
		public function get isActive():Boolean
		{
			return _isActive;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function get parent():IProject
		{
			return _parent;
		}
		
		public function get releases():Object
		{
			return _releases;
		}
		
		public function get startDate():Date
		{
			return _startDate;
		}
		
	}
}