package com.jgibson.ontimeair.OnTimeApi.models
{
	import com.bealearts.collection.VectorCollection;
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;
	import com.jgibson.ontimeair.OnTimeApi.gateways.ProjectGateway;
	import com.jgibson.ontimeair.OnTimeApi.interfaces.IProject;
	import com.jgibson.ontimeair.OnTimeApi.interfaces.IProjectCollection;
	import com.jgibson.ontimeair.OnTimeApi.interfaces.IProjectGateway;
	import com.jgibson.ontimeair.models.ILoadable;
	import com.jgibson.ontimeair.models.LoadStatus;
	
	import mx.collections.ArrayCollection;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;

	
	public class ProjectCollection extends VectorCollection implements IProjectCollection,ILoadable 
	{
		
		private var _loading:ISignal;
		private var _loaded:ISignal;
		private var _loadStatus:LoadStatus;
		private var _projectId:String;
		private var _gateway:IProjectGateway;
		
		public function ProjectCollection(projectId:String = null):void
		{
			_loadStatus = LoadStatus.GHOST;
			_loaded = new Signal();
			_loading = new Signal();
			_projectId = projectId;
			
			_gateway = new ProjectGateway();
		}
		
		/* Using a load method on this class to allow external classes time to set up listeners, there might be a better way */
		public function load(api:IOnTime):void
		{
			ILoadable(_gateway).loaded.add(projectsLoaded);
			_gateway.load(_projectId,api);
		}
		
		/* 	This is used to hydrate collections from a parent api call.
		*	The parent call will have returned from the api with children projects in tow.
		*	I dont like doing it this way so it is an area ripe for refactoring :-)
		*/
		public function externalLoad(collection:ArrayCollection):void
		{
			trace("Checking for Child projects");
			projectsLoaded(collection);
		}
		
		private function projectsLoaded(resultProjects:ArrayCollection):void
		{
			var projects:VectorCollection = new VectorCollection(new Vector.<IProject>);
			for each (var resultProject:Object in resultProjects) 
			{
				var project:IProject;
				project = new Project(resultProject);
				trace("	Adding project: ", project.name);
				projects.addItem(project);
			}
			
			if (projects.length > 0)
				trace("Child Projects Loaded: ", projects.length);
			else
				trace("No projects loaded");
			this.addAll(projects);
			_loadStatus = LoadStatus.LOADED;
			_loaded.dispatch();
		}
		
		public function get loadStatus():LoadStatus
		{
			return _loadStatus;
		}
		
		public function get loaded():ISignal
		{
			return _loaded;
		}
		
		public function get loading():ISignal
		{
			return _loading;
		}
		
		
	}
}