package com.jgibson.ontimeair.OnTimeApi
{
	public class SessionData implements ISessionData
	{
		private var _accessToken:String;
		private var _firstName:String;
		private var _lastName:String;
		private var _email:String;
		private var _userId:String;
		private var _tokenType:String;
		public function SessionData(requestData:Object)
		{
			var attributes:Object = requestData;
			
			_accessToken = attributes["access_token"];	
			_firstName = attributes.data["first_name"];
			_lastName = attributes.data["last_name"];
			_userId = attributes.data["id"];
			_email = attributes.data["email"];
			_tokenType = attributes["token_type"];
			
		}
		
		public function get accessToken():String
		{
			return _accessToken;
		}
		
		public function get firstName():String
		{
			return _firstName;
		}
		
		public function get lastName():String
		{
			return _lastName;
		}
		
		public function get email():String
		{
			return _email;
		}
		
		public function get userId():String
		{
			return _userId;
		}
		
		public function get tokenType():String
		{
			return _tokenType;
		}
	}
}