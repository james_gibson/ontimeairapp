package com.jgibson.ontimeair.OnTimeApi
{
	public class Settings implements ISettings
	{
		private var _clientSecret:String;
		private var _clientId:String;
		private var _onTimeUrl:String;
		private var _loginInfo:ILogin;
		public function Settings(OnTimeUrl:String, ClientId:String, ClientSecret:String)
		{
			_onTimeUrl = OnTimeUrl;
			_clientId = ClientId;
			_clientSecret = ClientSecret;
		}
		
		[Bindable]
		public function get loginInfo():ILogin
		{
			return _loginInfo;
		}
		
		public function set loginInfo(loginEntries:ILogin):void
		{
			_loginInfo = loginEntries;
		}
		
		public function get clientId():String
		{
			return _clientId;
		}
		
		public function get clientSecret():String
		{
			return _clientSecret;
		}
		
		public function get onTimeUrl():String
		{
			return _onTimeUrl;
		}
		
		
	}
}