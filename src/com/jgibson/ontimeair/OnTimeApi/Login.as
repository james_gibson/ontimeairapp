package com.jgibson.ontimeair.OnTimeApi
{
	public class Login implements ILogin
	{
		private var _username:String;
		private var _password:String;
		public function Login(usernameEntry:String, passwordEntry:String)
		{
			_username = usernameEntry;
			_password = passwordEntry;
		}
		
		public function get username():String
		{
			return _username;
		}
		
		public function get password():String
		{
			return _password;
		}
	}
}