package com.jgibson.ontimeair.OnTimeApi
{
	public interface ILogin
	{
		//Consider making this secure somehow?  needs research
		function get username():String;
		function get password():String;
	}
}