package com.jgibson.ontimeair.OnTimeApi
{
	public interface IOnTime
	{
		function get hasAccessToken():Boolean;
		
		function obtainAccessTokenFromUsernamePassword():void;
		function get settings():ISettings;
		function set settings(value:ISettings):void;
		function get sessionData():ISessionData;
		/* CRUD */
		function Create():void;
		function Read():void;
		function Update():void;
		function Delete(resource:String, id:int):void;
	}
}