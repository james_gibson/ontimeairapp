package com.jgibson.ontimeair.presentationModels
{
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;
	import com.jgibson.ontimeair.OnTimeApi.interfaces.IProjectCollection;
	import com.jgibson.ontimeair.OnTimeApi.models.ProjectCollection;
	import com.jgibson.ontimeair.models.ILoadable;
	
	import spark.components.supportClasses.SkinnableComponent;
	
	public class AuthenticatedViewPM extends SkinnableComponent
	{
		private var _onTime:IOnTime;
		private var _projectCollection:IProjectCollection;
		public function AuthenticatedViewPM(api:IOnTime)
		{
			super();
			_onTime = api;
			
			//Temp collection...
			
			_projectCollection = new ProjectCollection();
			ILoadable(_onTime).loaded.add(apiLoaded);
		}
		
		private function apiLoaded():void
		{
			ILoadable(_projectCollection).loaded.add(onProjectCollectionHydration);
			_projectCollection.load(_onTime);
		}		
		
		private function onProjectCollectionHydration():void
		{
			trace("Project Collection Hydrated");
			projectCollection = _projectCollection;
		}
		
		[Bindable]
		public function get projectCollection():IProjectCollection
		{
			return _projectCollection;
		}
		
		public function set projectCollection(value:IProjectCollection):void
		{
			_projectCollection = value;
		}
	}
}