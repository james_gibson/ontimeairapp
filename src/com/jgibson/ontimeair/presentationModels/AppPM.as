package com.jgibson.ontimeair.presentationModels
{
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;
	import com.jgibson.ontimeair.OnTimeApi.ISettings;
	import com.jgibson.ontimeair.OnTimeApi.OnTime;
	import com.jgibson.ontimeair.OnTimeApi.Settings;
	import com.jgibson.ontimeair.models.ILoadable;
	
	import spark.components.supportClasses.SkinnableComponent;

	public class AppPM extends SkinnableComponent
	{
		protected var onTime:IOnTime;
		private var _onTimeSettings:ISettings;
		private var _loginViewPM:LoginViewPM;
		private var _url:String;
		private var _clientId:String;
		private var _clientSecret:String;
		private var _authenticatedViewPM:AuthenticatedViewPM;
		private var _authenticated:Boolean;
		public function AppPM()
		{
			onTime = new OnTime();
			
			//Get these from a config file
			_url = "https://gisinctesting.ontimenow.com"+"/api";
			_clientId = "4e5e4d2c-6514-4bb2-8b3a-ec0abf11e961";
			_clientSecret = "b16879f1-8ab9-41ad-80e5-f636259749af";
			_onTimeSettings = new Settings(_url,_clientId,_clientSecret);
			
			onTime.settings = _onTimeSettings;
			var loadableApi:ILoadable = ILoadable(onTime);
			
			loadableApi.loaded.add(onConnectionSuccess);
			authenticatedViewPM = new AuthenticatedViewPM(onTime);
			loginViewPM = new LoginViewPM(onTime);
		}
		
		private function onConnectionSuccess():void
		{
			if (onTime.hasAccessToken)
			{
				authenticated = true;
				invalidateSkinState();
			}
			return;
		}
		
		[Bindable]
		public function get loginViewPM():LoginViewPM
		{
			return _loginViewPM;
		}
		
		public function set loginViewPM(value:LoginViewPM):void
		{
			_loginViewPM = value;
		}
		
		[Bindable]
		public function get authenticatedViewPM():AuthenticatedViewPM
		{
			return _authenticatedViewPM;
		}
		
		public function set authenticatedViewPM(value:AuthenticatedViewPM):void
		{
			_authenticatedViewPM = value;
		}
		
		[Bindable]
		public function get authenticated():Boolean
		{
			return _authenticated;
		}
		
		private function set authenticated(value:Boolean):void
		{
			_authenticated = value;
		}
	}
}