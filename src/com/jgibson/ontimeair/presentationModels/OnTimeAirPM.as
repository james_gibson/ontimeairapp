package com.jgibson.ontimeair.presentationModels
{
	import com.jgibson.ontimeair.OnTimeApi.ILogin;
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;
	import com.jgibson.ontimeair.OnTimeApi.ISettings;
	import com.jgibson.ontimeair.OnTimeApi.Login;
	import com.jgibson.ontimeair.OnTimeApi.OnTime;
	import com.jgibson.ontimeair.OnTimeApi.Settings;
	import com.jgibson.ontimeair.models.ILoadable;
	import com.jgibson.ontimeair.models.LoadStatus;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;
	
	import spark.components.supportClasses.SkinnableComponent;

	public class OnTimeAirPM extends SkinnableComponent implements ILoadable
	{
		private var _loadStatus:LoadStatus;
		private var _loaded:ISignal;
		private var _loading:ISignal;
		
		public function OnTimeAirPM()
		{
			_loadStatus = LoadStatus.GHOST;
			_loading = new Signal();
			_loaded = new Signal();
			
		}
		
		
		override protected function getCurrentSkinState():String 
		{
			trace("state checked");
			return "authenticated";
		}
		
		
		
		/* ILoadable */
		public function get loadStatus():LoadStatus
		{
			return _loadStatus;
		}
		
		public function get loaded():ISignal
		{
			return _loaded;
		}
		
		public function get loading():ISignal
		{
			return _loading;
		}
	}
}