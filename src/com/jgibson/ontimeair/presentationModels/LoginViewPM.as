package com.jgibson.ontimeair.presentationModels
{
	import com.jgibson.ontimeair.OnTimeApi.ILogin;
	import com.jgibson.ontimeair.OnTimeApi.IOnTime;
	import com.jgibson.ontimeair.OnTimeApi.ISettings;
	import com.jgibson.ontimeair.OnTimeApi.Login;
	import com.jgibson.ontimeair.OnTimeApi.OnTime;
	import com.jgibson.ontimeair.OnTimeApi.Settings;
	import com.jgibson.ontimeair.models.ILoadable;
	import com.jgibson.ontimeair.models.LoadStatus;
	
	import flash.events.MouseEvent;
	
	import spark.components.supportClasses.SkinnableComponent;
	
	import org.osflash.signals.ISignal;
	import org.osflash.signals.Signal;

	public class LoginViewPM extends SkinnableComponent implements ILoadable
	{
		private var _loadStatus:LoadStatus;
		private var _loaded:ISignal;
		private var _loading:ISignal;
		private var _username:String;
		private var _clientSecret:String;
		private var _clientId:String;
		private var _url:String;
		private var _password:String;
		private var _api:IOnTime;
		
		
		public function LoginViewPM(api:IOnTime)
		{
			
			_api = api;
			_loadStatus = LoadStatus.GHOST;
			_loading = new Signal();
			_loaded = new Signal();
		}
		
		public function processLoginClick(event:MouseEvent):void
		{
			trace("Connect requested");
			_loadStatus = LoadStatus.LOADING;
			invalidateSkinState();
			connectToApi();
		}
		
		
		private function connectToApi():void
		{
			var loginInfo:ILogin = new Login(_username,_password);
			var loadableApi:ILoadable = ILoadable(_api);
			_api.settings.loginInfo = loginInfo;
			loadableApi.loaded.add(connectedToApi);
			
			if (loadableApi.loadStatus == LoadStatus.LOADED)
				connectedToApi();
			_api.obtainAccessTokenFromUsernamePassword();
			
			
		}
		
		private function connectedToApi():void
		{
			_loadStatus = LoadStatus.LOADED;
			loaded.dispatch();
			invalidateSkinState();
			trace("Connected to api!!!");
		}		
		
		public function set usernameEntry(value:String):void
		{
			_username = value;	
		}
		
		[Bindable]
		public function get usernameEntry():String
		{
			return _username;	
		}
		
		public function set passwordEntry(value:String):void
		{
			_password = value;	
		}
		
		[Bindable]
		public function get passwordEntry():String
		{
			return _password;	
		}
		
		public function get loadStatus():LoadStatus
		{
			return _loadStatus;
		}
		
		public function get loaded():ISignal
		{
			return _loaded;
		}
		
		public function get loading():ISignal
		{
			return _loading;
		}
		
	}
}